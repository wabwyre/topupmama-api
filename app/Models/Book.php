<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Book extends Model
{
    protected $fillable = ['name', 'isbn', 'authors', 'pages', 'publisher', 'country', 'media_type', 'released'];

    public function scopeReleasedAscending($query){
        return $query->orderBy('released','ASC');
    }  
    
    public function comments() {
        return $this->hasMany(Comment::class);
      }

    
}
