<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{    

    /**
     * get all comment paginate
     *
     * @param  Request $request per_page=10&page=1
     * @return void
     */
    public function all(Request $request){

        /* define record per page  */
        $per_page = (int) ($request->per_page ?? 10);

        /* limit per page not greater then 50 */
        if ($per_page > 50 ) {

            $per_page = 50;
        }

        return $this->core->setResponse('success', 'Get comments', Comment::idDescending()->paginate($per_page));
    }
    
    /**
     * show comment by id
     *
     * @param  string $id
     * @return JsonResponse
     */
    public function show($id){

        if (! $comment = Comment::find($id)) {

            return $this->core->setResponse('error', 'comment Not Found', NULL, FALSE, 404);
        }

        return $this->core->setResponse('success', 'comment Found', $comment);
    }
    
    /**
     * create comment
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function create(Request $request) {

        /* validation requirement */
        $validator = $this->validation('create', $request);

        if ($validator->fails()) {

            return $this->core->setResponse('error', $validator->messages()->first(), NULL, false , 400  );
        }

        // $comment = Comment::create($request->all());
        $clientIP = request()->getClientIp();
        $com = $request->input('comment');
        $book_id = $request->input('book_id');
    
        $comment = Comment::create([
            'comment'=> $com,
            'book_id'=> $book_id,
            'ip_address'=> $clientIP
        ]);

        return $this->core->setResponse('success', 'comment Created', $comment);

    }
    
    /**
     * update comment
     *
     * @param  Request $request
     * @param  string $id
     * @return JsonResponse
     */
    public function update(Request $request, $id) {
        
        /* validation requirement */
        $validator = $this->validation('update', $request);

        if ($validator->fails()) {

            return $this->core->setResponse('error', $validator->messages()->first(), NULL, false , 400  );
        }

        $comment = Comment::find($id);

        $comment->fill($request->only(['comment','book_id',]))->save();

        return $this->core->setResponse('success', 'comment Updated', $comment);
        
    }
    
   
    /**
     * delete comment
     *
     * @param  string $id
     * @return JsonResponse
     */
    public function delete($id) {
        
        if (!$comment = Comment::find($id)) {

            return $this->core->setResponse('error', 'comment Not Found', NULL, FALSE, 404);
        }

        $comment->delete();

        return $this->core->setResponse('success', 'comment deleted');
        
    }


    /**
     * validation requirement
     *
     * @param  string $type
     * @param  request $request
     * @return object
     */
    private function validation($type = null, $request) {

        switch ($type) {

            case 'create' || 'update':

                $validator = [
                    'comment' => 'required|max:500|min:10',
                    'book_id' => 'required',
                ];
                
                break;

            default:
                
                $validator = [];
        }

        return Validator::make($request->all(), $validator);
    }

}
