<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class BookController extends Controller
{
    /**
     * get all book paginate
     *
     * @param  Request $request per_page=10&page=1
     * @return void
     */
    public function all(Request $request){

        /* define record per page  */
        $per_page = (int) ($request->per_page ?? 10);

        /* limit per page not greater then 50 */
        if ($per_page > 50 ) {

            $per_page = 50;
        }

        return $this->core->setResponse('success', 'Get books', Book::withCount(['comments'])->releasedAscending()->paginate($per_page));
    }
    
    /**
     * show book by id
     *
     * @param  string $id
     * @return JsonResponse
     */
    public function show($id){

        if (! $book = Book::find($id)) {

            return $this->core->setResponse('error', 'Book Not Found', NULL, FALSE, 404);
        }

        return $this->core->setResponse('success', 'Book Found', $book);
    }

    /**
     * save book data from https://www.anapioficeandfire.com/api/books
     */
    public function saveBookAPIData(){
        try 
       {
        $response = Http::get('https://www.anapioficeandfire.com/api/books',['page'=> '1', 'pageSize'=>'20']);
       
        $arrays = $response->body();
        $data= json_decode($response->getBody(), true);

        //  return $this->core->setResponse('success', 'data', $data);
        foreach($data as $key=>$value){
            $Book = new Book;
            $Book->name = $value['name'];
            $Book->isbn = $value['isbn'];
            $Book->authors = json_encode($value['authors']);
            $Book->pages = $value['numberOfPages'];
            $Book->publisher = $value['publisher'];
            $Book->country = $value['country'];
            $Book->media_type = $value['mediaType'];
            $Book->released = $value['released'];
            $Book->save();
        }
    
         //return successful response
         return $this->core->setResponse('success', 'Books records saved!');

       } 
       catch (\Illuminate\Database\QueryException $ex) 
       {
         //return error message
         return $this->core->setResponse('error', $ $ex->getMessage(), NULL, false , 400  );
         
       }

    }
}
