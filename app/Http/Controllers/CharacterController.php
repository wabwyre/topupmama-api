<?php

namespace App\Http\Controllers;

use App\Models\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class CharacterController extends Controller
{
   /**
     * get all character paginate
     *
     * @param  Request $request per_page=10&page=1
     * @return void
     */
    public function all(Request $request){

        /* define record per page  */
        $per_page = (int) ($request->per_page ?? 10);

        /* limit per page not greater then 50 */
        if ($per_page > 50 ) {

            $per_page = 50;
        }

        return $this->core->setResponse('success', 'Get characters', Character::nameAscending()->paginate($per_page));
    }
    
      /**
     * show character by id
     *
     * @param  string $id
     * @return JsonResponse
     */
    public function show($id){

        if (! $character = Character::find($id)) {

            return $this->core->setResponse('error', 'Character Not Found', NULL, FALSE, 404);
        }

        return $this->core->setResponse('success', 'Character Found', $character);
    }

    /**
     * search character based on gender paginate
     *
     * @param  Request $request per_page=10&page=1
     * @return void
     */
    public function search(Request $request)
    {
      /* define record per page  */
      $per_page = (int) ($request->per_page ?? 10);

      /* limit per page not greater then 50 */
      if ($per_page > 50 ) {

          $per_page = 50;
      }

      $search = $request->search;
      try {
        $data = DB::table("characters")->where("gender", $search)->paginate($per_page)->get();

        return $this->core->setResponse('success', 'get characters success', $data);

        } catch (\Illuminate\Database\QueryException $ex) {

            return $this->core->setResponse('error', $ $ex->getMessage(), NULL, false , 400  ); 

        }

    }

    /**
     * Save Characters from https://anapioficeandfire.com/api/characters
     */

    public function saveCharacterApiData(){
        try 
        {
            $totalItems = 2140;
            $itemsPerPage = 50;
            $limit = ceil($totalItems / $itemsPerPage);
            $i = 1;
            do{
                $i++;
               
                 $response = Http::get('https://anapioficeandfire.com/api/characters',['page'=> $i, 'pageSize'=>'50']);
                 $data= json_decode($response->getBody(), true);
                   foreach($data as $key=>$value){ 
                    // $book = $value['books'];
                   
                    // return $this->core->setResponse('success', 'data', $book);
                    $Character = new Character;
                    $Character->name = $value['name'];
                    $Character->gender = $value['gender'];
                    $Character->culture = $value['culture'];
                    $Character->born = $value['born'];
                    $Character->died = $value['died'];
                    $Character->titles = json_encode($value['titles']);
                    $Character->aliases = json_encode($value['aliases']);
                    $Character->playedBy = json_encode($value['playedBy']);
                    $Character->save();
                }
           }
            while($i <= $limit);
               // return successful response
                return $this->core->setResponse('success', 'Characters records saved!');
        } 
        catch (\Illuminate\Database\QueryException $ex) 
        {
            //return error message
             return $this->core->setResponse('error', $ $ex->getMessage(), NULL, false , 400  );
           
        }
    
    }

}
