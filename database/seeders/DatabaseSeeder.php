<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* create default user */
        User::create([
            'firstname' => 'System',
            'lastname' => 'Admin',
            'email' => 'system.admin@email.com',
            'role' => 'Admin',
            'password' =>  Hash::make('secret'),
        ]);

    }
}
