<?php 

/* Book group */
$router->group(['prefix' => 'book', 'as' => 'book'], function () use ($router) {

    /* restrict route */
    $router->group(['middleware' => 'auth'], function () use ($router) {

        /* save book api data */
        $router->get('/save-book', [ 'as' => 'saveBook', 'uses' => 'BookController@saveBookAPIData']);   

        /* all Books */
        $router->get('/all', [ 'as' => 'all', 'uses' => 'BookController@all']);

        /* show Book by id */
        $router->get('/{id}', [ 'as' => 'show', 'uses' => 'BookController@show']);
 
    
    });

    
});