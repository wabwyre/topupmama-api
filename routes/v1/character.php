<?php 

/* character group */
$router->group(['prefix' => 'character', 'as' => 'character'], function () use ($router) {

    /* restrict route */
    $router->group(['middleware' => 'auth'], function () use ($router) {

        /* save character api data */
        $router->get('/save-character', [ 'as' => 'saveBook', 'uses' => 'CharacterController@saveCharacterApiData']);   

        /* all characters */
        $router->get('/all', [ 'as' => 'all', 'uses' => 'CharacterController@all']);

        /* search characters */
        $router->get('/search-characters',  ['as' => 'searchCharacters', 'uses' =>  'CharacterController@search']);

        /* show character by id */
        $router->get('/{id}', [ 'as' => 'show', 'uses' => 'CharacterController@show']);
 
    
    });

    
});