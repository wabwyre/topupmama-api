<?php 

/* comment group */
$router->group(['prefix' => 'comment', 'as' => 'comment'], function () use ($router) {

    /* restrict route */
    $router->group(['middleware' => 'auth'], function () use ($router) {

        /* all comments */
        $router->get('/all', [ 'as' => 'all', 'uses' => 'CommentController@all']);

        /* create comments */
        $router->post('/create', [ 'as' => 'create', 'uses' => 'CommentController@create']);

        /* show comments by id */
        $router->get('/{id}', [ 'as' => 'show', 'uses' => 'CommentController@show']);
  
        /* update comments */
        $router->patch('/{id}/update', [ 'as' => 'update', 'uses' => 'CommentController@update']);

        /* delete comments */
        $router->delete('/{id}/delete', [ 'as' => 'delete', 'uses' => 'CommentController@delete']);
    
    });

    
});