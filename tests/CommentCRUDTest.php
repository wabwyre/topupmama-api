<?php

use App\Models\Comment;
use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;

class CommentCRUDTest extends TestCase
{
    use DatabaseMigrations;

    public $token;

    public function setUp(): void {

        parent::setUp();

        Artisan::call('migrate:fresh',['--seed' => true]);

    }

    /**
     * show all comments
     *
     * @return void
     */
    public function test_show_all()
    {
        $this->get( route('v1.comment.all', ['page' => 1, 'per_page' => 10]) );

        $this->response->assertJson(['success' => true]);
    }

    /**
     * show comment by id
     *
     * @return void
     */
    public function test_get_comment_by_id()
    {
        $comment = Comment::first();

        $this->get( route('v1.comment.show', ['id' => $comment->id ] ) );

        $this->response->assertJson([
            'success' => 'Comment Found'
        ])->assertStatus(200);

    }

    
    /**
     * create comment
     *
     * @return void
     */
    public function test_create_comment() {

        $this->setToken();

        $this->post( route('v1.comment.create'), [
            'comment' => 'Test comment',
            'ip_address' => '127.0. 0.1',
            'book_id' => '1',
        ]);

         /* validate required */
        $this->response->assertJson([
            'success' => true,
        ])->assertStatus(200);

    }
    
    /**
     * update comment
     *
     * @return void
     */
    public function test_update_comment() {

        $comment = comment::first();
        
        $this->setToken();

        $new_data = [
            'comment' => 'Test comment Updated',
            'ip_address' => '127.0. 0.1',
            'book_id' => '1',
        ];
        
        $this->patch( route('v1.comment.update', ['id' => $comment->id ]), $new_data);

        $this->response->assertJson([
            'success' => true,
        ])->assertStatus(200);

        /* validate if current comment is updated  */
        $new_comment = comment::find($comment->id);

        $this->assertTrue(
            !array_diff($new_data, $new_comment->toArray())
        );

    }
    
    /**
     * delete comment
     *
     * @return void
     */
    public function test_delete_comment() {

        $comment = comment::first();
        
        $this->setToken();

        $this->delete( route('v1.comment.delete' ,['id' => $comment->id ]));

        $this->response->assertJson([
            'success' => true,
        ])->assertStatus(200);

    }

    /**
     * set token for auth
     *
     * @return void
     */
    private function setToken() {

        /* valid register */
        $request = $this->post('/v1/register', [
            'firstname' => 'System',
            'lastname' => 'Admin',
            'email' => 'system.admin@email.com',
            'password' => 'secret',
        ]);

        /* valid login */
        $this->post('/v1/login', [
            'email' => 'system.admin@email.com',
            'password' => 'secret',
        ]);

    }
    
}